



// Functions


// Parameters and Arguements



/*function printInput(){

	let nickname = ("Enter your nickname");

	console.log('Hi,' + nickname);
}


printInput();*/


// This function has parameter and arguement

function printName(name){
	console.log ("My name is " + name);
}


printName("juana");  // --> "juana" is the arguement


printName("Jhemmo");



let sampleVariable = "Yui";

printName(sampleVariable);



function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of "+ num + "divisible by 8 is :" + remainder);

	let isDivisibleby8 = remainder === 0;
	console.log("Is " + num + " divisible by 8? " );
	console.log(isDivisibleby8);
}	

checkDivisibilityBy8(64);


function  argumentFunction(){
	console.log("This function was passed as an argument before the message.")
}

function invokeFunction(argumentFunction){
	argumentFunction();
}


invokeFunction(argumentFunction);


function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}

createFullName("Jhemmo","Villena","Cocosa")




let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";



function printFullName(middleName, firstName, lastName){
	console.log(firstName + ' ' + middleName + ' ' + lastName);
}



printFullName("Juan","Dela","Cruz");


// Return Statement


function returnFullName (firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;

	console.log('Test console');

}

let completeName = returnFullName('Juan','Dela','Cruz');
console.log(completeName);



console.log(returnFullName(firstName, middleName, lastName));


function returnAddress (city, country){
	let fullAddress = city + ', ' + country;
	return fullAddress;

	console.log("This will not be displayed");
}


let myAddress = returnAddress("Batangas City","Philippines");

console.log(myAddress);



function printPlayerInfo (username, level, job){

	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);

}


/*let user1 =*/ printPlayerInfo("Knight_white","95","Paladin");

// console.log(user1);